# My Todo Lists

- Created by Jane Sundermann - Full Stack Software Engineer

# Intended market

My Todo Lists was created for anyone who wants to simple way to manage their todos and todo lists.

# Functionality

- Users of My Todo Lists can:
     - Create new todo lists
     - Create new todo items
     - Add or delete items from lists
     - Set due dates for items
     - Change due dates for items
     - Set completion status for items
     - Change completion status for items
     - View all todo lists and number of items on each list
     - View todo list and details about each item on list (e.g., due date, status)

## Project Initialization

To use the application on your local machine, please follow these steps:

1. Clone the repository down to your local machine.
2. CD into the new project directory.
3. Create and activate a virtual environment.
4. Install django and other dependencies.
5. Run 'python manage.py runserver'.
6. Navigate in your browser to https://localhost:8000/todos
