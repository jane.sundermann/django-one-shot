from django.contrib import admin
from todos.models import TodoList, TodoItem

# Register your models here.
# "When registering a model, make sure you do 2 things:"
# "1) Create a class that inherits from admin.ModelAdmin"
# "2)Register the class along with your model using admin.site.register()"

#"Register the TodoList model with the admin so that you can see it in
# the DJango admin site. Have it display the name and id as columns in
# the model's list display"
@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
    ]

#"Register the TodoItem model within the admin so that you can see it in
# the Django admin site. Have it display the task, due_date, and whether
# or not the todo item is completed"
@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = [
        "task",
        "due_date",
        "is_completed",
    ]