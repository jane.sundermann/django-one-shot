from django.db import models

# Creates model for the todo list, with requested properties
class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    # Converts model into a string when printed or displayed in admin
    def __str__(self):
        return self.name


# Creates model for the todo item, with requested properties
class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(
        null=True,
        blank=True,
    )
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(
        TodoList,
        related_name="items",
        on_delete=models.CASCADE,
    )

    # Converts model into a string when printed or displayed in admin
    def __str__(self):
        return self.task
    