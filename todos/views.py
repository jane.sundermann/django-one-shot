# do your imports here
from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoListUpdateForm, TodoListDeleteForm, TodoItemForm, TodoItemUpdateForm

# Create your views here.

# “Create a view that will get all of the instances of the TodoList model
# and put them in the context for the template”.
def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_list_list": todo_lists,
    }
    return render(request, "todos/list.html", context)

# creates view that shows details of a particular to-do list including its tasks

def todo_list_detail(request, id):
    # todo_items = TodoItem.objects.all()
    todo_items = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_items,
    }
    return render(request, "todos/detail.html", context)

# Feature 9, create view for TodoList model that will show the name field
# in the form and handle the for submission to create a new TodoList
def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            id = todolist.id
            todolist.save()
            return redirect("todo_list_detail", id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

# Feature 10, Create an update view for the TodoList model that
# will show the name. field in the form and handle the form submission
# to change an existing TodoList.
# If the to-do list is successfully edited, it should redirect to the
# detail page for that to-do list.
def todo_list_update(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListUpdateForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListUpdateForm(instance=todolist)
    context = {
        "todo_object": todolist,
        "form": form,
    }
    return render(request, "todos/update.html", context)

# Feature 11, Create a delete view for the TodoList model that will show
# a delete button and handle the form submission to delete an existing TodoList
# if the todo list is successfully deleted, it should redirect to the to-do
# list view


def todo_list_delete(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

# Feature 12, create view for TodoList model that will show the "task" field
# in the form and handle the for submission to create a new TodoItem.
# if the todo item is successfully created, it should redirect to the detail
# page for that todo item.


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            id = item.list.id
            item.save()
            return redirect("todo_list_detail", id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)

# Feature 13, create update view for TodoItem model that will show the "task"
# field, the due_date field, an is_completed checkbox, and select list showing
# the to-do lists in the form and handle the form submission to change an
# existing TodoItem
# If the to-do item is successfully edited, it should redirect to the detail
# page for the to-do list


def todo_item_update(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemUpdateForm(request.POST, instance=todoitem)
        if form.is_valid():
            todoitem = form.save()
            todoitem.save()
            return redirect("todo_list_detail", id=todoitem.list.id)
    else:
        form = TodoItemUpdateForm(instance=todoitem)
    context = {
        "form": form,
    }
    return render(request, "todos/update_item.html", context)
